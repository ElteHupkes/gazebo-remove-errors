#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <queue>

namespace gazebo
{
class BreakPlugin : public WorldPlugin {
public:
	BreakPlugin() : WorldPlugin() {}

	void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf) {
		std::cout << "Plugin loaded." << std::endl;

		counter_ = 0;

		world_ = _world;

		// Pause the world
		world_->SetPaused(true);

		// Create a new transport node for advertising / subscribing
		node_.reset(new transport::Node());
		node_->Init();

		// Subscribe to model insertion
		modelSub_ = node_->Subscribe("~/model/info", &BreakPlugin::OnModel, this);

		// Subscribe to contacts
		contactsSub_ = node_->Subscribe("~/physics/contacts", &BreakPlugin::OnContacts, this);

		this->AddNew();
	}

	void AddNew() {
		// The sphere model from the world plugin example - remove works
		// fine with one link, the example crashes with two.
		world_->InsertModelFile("model://break_bot");
		std::cout << "Added model #" << counter_ << std::endl;
	}

	void OnModel(ConstModelPtr & msg) {
		// Unpause the world for a step of processing
		world_->SetPaused(false);
		std::cout << "Models in world: " << world_->GetModelCount() << std::endl;
	}

	void OnContacts(ConstContactsPtr &msg) {
		// Handle contacts
		world_->SetPaused(true);
		world_->RemoveModel("sphere");
		std::cout << "Model removed." << std::endl;
	}

private:
	// Store a pointer to the world
	physics::WorldPtr world_;

	// Request counter
	int counter_;

	// Transport node
	transport::NodePtr node_;

	// Subscribers / publishers
	transport::SubscriberPtr modelSub_;
	transport::SubscriberPtr contactsSub_;
};

GZ_REGISTER_WORLD_PLUGIN(BreakPlugin)

}
